import Vue from 'vue';
import VueI18n from 'vue-i18n';

import ptBr from './languages/pt-br.json';
import esPe from './languages/es-pe.json';
import enUs from './languages/en-us.json';

export const defaultLocale = 'pt-br';

export const languages = {
  'pt-br': ptBr,
  es: esPe,
  en: enUs,
};

Vue.use(VueI18n);

const messages = Object.assign(languages);

const i18n = new VueI18n({
  locale: defaultLocale,
  fallbackLocale: 'es',
  messages,
});

export default i18n;
